<?php

include '../models/Shopper.php';
include '../models/Order.php';
include '../handlers/ApiRequestHandler.php';

class FishaApiService extends ApiRequestHandler
{
    protected $host = 'fisha.api.co.il';
    protected $key = '';
    protected $password = '';

    public function __construct() {
        parent::__construct($this->key, $this->password);
    }

    /**
     * createNewShopper
     *
     * @param Shopper $shopper
     * @return int $fishaShopperId
     */
    public function createNewShopper(Shopper $shopper)
    {
        $url = $this->host . '/CreateNewShopper';
        $shopperArr = $shopper->toArray();
        
        $this->makePostRequest($url, ['shopper' => $shopperArr])->execute();

        return (int) $this->getResponseData('fishaShopperId');
    }

    /**
     * updateShopper
     *
     * @param Shopper $shopper
     * @return void
     */
    public function updateShopper(Shopper $shopper)
    {
        $url = $this->host . '/UpdateShopper';
        $shopperArr = $shopper->toArray();
        $this->makePostRequest($url, ['shopper' => $shopperArr])->execute();
    }

    /**
     * getShopperById
     *
     * @param int $fishaShopperId
     * @return Shopper $shopper
     */
    public function getShopperById($fishaShopperId)
    {
        $url = $this->host . '/getShopperById';
        $successCallback = function($responseArr) {
            if (!empty($responseArr)) {
                $shopper = new Shopper();
                $shopper->fillEntity($responseArr);
                return ['shopper' => $shopper];
            }
        };

        $this->makePostRequest($url, ['fishaShopperId' => $fishaShopperId])
            ->onSuccess($successCallback)
            ->execute();

        return $this->getResponseData('shopper');
    }

    /**
     * createNewOrder
     *
     * @param Order $order
     * @return int $fishaOrderId
     */
    public function createNewOrder(Order $order)
    {
        $url = $this->host . '/createNewOrder';
        $orderArr = $order->toArray();
        
        $this->makePostRequest($url, ['order' => $orderArr])->execute();

        return (int) $this->getResponseData('fishaOrderId');
    }

    /**
     * getOrders
     *
     * @param Shopper $shopper
     * @return $listOfOrders
     */
    public function getOrders(Shopper $shopper)
    {
        $url = $this->host . '/getOrders';
        $fishaShopperId = $shopper->getProperty('id');

        $this->makePostRequest($url, ['fishaShopperId' => $fishaShopperId])->execute();
        
        return $this->getResponseData();
    }

    protected function onSuccess($callback = null)
    {
        if (!is_null($callback)) {
            $this->responseArr = $callback($this->responseArr);
        }

        return $this;
    }
}
