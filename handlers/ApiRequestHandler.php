<?php

class ApiRequestHandler
{
    private $ch;

    protected $credentialsArr;

    protected $responseArr;

    /**
     * ApiRequestService constructor.
     * @param $key
     * @param $password
     */
    public function __construct($key, $password)
    {
        $this->credentialsArr = [
            'key' => $key,
            'password' => $password,
        ];
    }

    /**
     * @param $url
     * @param array $postFieldsArr option
     * @return mixed
     */
    public function makePostRequest($url, $postFieldsArr = [])
    {
        $this->addCredentials($postFieldsArr);
        $postFields = http_build_query($postFieldsArr);

        $this->ch = curl_init($url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postFields);

        $this->ch = $this->ch;

        return $this;
    }

    /**
     * execute the current curl request
     */
    public function execute()
    {
        try {
            // execute
            $res = curl_exec($this->ch);
            // close the connection, release used resources
            curl_close($this->ch);
        } catch (Exception $e) {
            ErrorHandler::newError($e->getMessage());
        }

        $this->responseArr = json_decode($res, true);

        return $this;
    }

    protected function addCredentials(&$postFieldsArr)
    {
        foreach ($this->credentialsArr as $key => $val) {
            if (!empty($val)) {
                $postFieldsArr[$key] = $val;
            } else {
                ErrorHandler::newError('Missing credentials');
            }
        }
    }

    protected function getResponseData($key = '')
    {
        if (empty($key)) {
            return $this->responseArr;
        } elseif (isset($this->responseArr[$key])) {
            return $this->responseArr[$key];
        } else {
            ErrorHandler::newError('Response data does not contain this key: ' . $key);
        }
    }
}