<?php

class Model
{
    protected $className;


    public function __construct()
    {
        $this->className = get_class($this);
    }

    public function fillEntity($propertiesArr)
    {
        foreach (array_keys($propertiesArr) as $property) {
            $this->setProperty($property, $propertiesArr[$property]);
        }
        return $this;
    }

    public function toArray()
    {
        $propertiesArr = [];
        
        $ref = new ReflectionClass($this->className);  

        foreach ($ref->getProperties() as $property) {
            $propertyName = $property->getName();
            $propertiesArr[$propertyName] = $this->getProperty($propertyName);
        }

        return $propertiesArr;
    }

    public function setProperty($propertyName, $value)
    {
        if (property_exists($this->className, $propertyName)) {
            $this->{$propertyName} = $value;
        }
    }

    public function getProperty($propertyName)
    {
        if (property_exists($this->className, $propertyName)) {
            return $this->{$propertyName};
        }
    }
}
