<?php

class Shopper extends Model
{
    protected $id;
    protected $email;
    protected $name;
    protected $lastName;
    protected $phone;
    protected $city;
    protected $street;
    protected $houseNumber;
    protected $last_update;

    public function __construct() {
        parent::__construct();
    }
}
